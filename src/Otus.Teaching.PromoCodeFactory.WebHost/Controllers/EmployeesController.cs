﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;

        public EmployeesController(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }

        /// <summary>
        /// Редактировать сотрудника
        /// </summary>
        /// <param name="id"></param>
        /// <param name="FirstName"></param>
        /// /// <param name="LastName"></param>
        /// <param name="email"></param>
        /// <param name="roles">Comma-separated list of roles</param>
        /// <returns></returns>
        [HttpPut("{id:guid}")]
        public async Task<IActionResult> EditEmployeeAsync(Guid id, string FirstName, string LastName, string email, string roles)//async
        {
            //get employee by id
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();

            //delete old employee from repository
            await DeleteEmployeeAsync(id);

            //edit employee
            employee.FirstName = FirstName;
            employee.LastName = LastName;
            employee.Email = email;
            employee.Roles = roles.SplitByRoles();

            //add edited employee to repository  
            await _employeeRepository.AddAsync(employee);

            return Ok(); 

        }

        /// <summary>
        /// Удалить сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteEmployeeAsync(Guid id)
        {
            var isDeleted = await _employeeRepository.DeleteByIdAsync(id);

            return isDeleted ? Ok()
                             : NotFound();

        }

        /// <summary>
        /// Создать нового сотрудника
        /// </summary>
        /// <param name="FirstName"></param>
        /// /// <param name="LastName"></param>
        /// <param name="email"></param>
        /// <param name="roles">Comma-separated list of roles</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateEmployeeAsync(string FirstName, string LastName, string email, string roles)
        {
            //create new employee
            var employee = new Employee
            {
                Id = Guid.NewGuid(),
                FirstName = FirstName,
                LastName = LastName,
                Email = email,
                Roles = roles.SplitByRoles()
            };

            //add new employee to repository  
            await _employeeRepository.AddAsync(employee);


            return Ok(); //$"New Employee {employee.FirstName}, guid={employee.Id} has been successfully created!";
        } 

        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();
            
            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }
    }
    public static class StringExtension
    {
        public static List<Role> SplitByRoles(this string strRoles)
        {
            return Regex.Split(strRoles, @"\W+")
                        .Select(s => FakeDataFactory.Roles.FirstOrDefault(r => r.Name == s))
                        .ToList();
        }

    }
}